import React, { Component } from "react";
import { Grid, Cell } from "react-mdl";
import Education from "./education";
import Experience from "./experience";
//import Skills from "./skills"; le utilise quand j'aurai de expérience
import avatar from "../avatar-gratuit.png";

class Parcours extends Component {
  render() {
    return (
      <div>
        <Grid>
          <Cell col={4}>
            <div style={{ textAlign: "center" }}>
              <img src={avatar} alt="avatar" style={{ height: "200px" }} />
            </div>

            <h2 style={{ paddingTop: "2em" }}>Triven Kadiata</h2>
            <h4 style={{ color: "grey" }}>développeur web junior</h4>
            <hr style={{ borderTop: "3px solid #833fb2", width: "50%" }} />
            <p>
              Le développeur web / la développeuse réalise l’ensemble des
              fonctionnalités techniques d’un site ou d'une application web.
              Technicien ou ingénieur, il ou elle conçoit des sites sur mesure
              ou adapte des solutions techniques existantes en fonction du
              projet et de la demande du client.
            </p>
            <hr style={{ borderTop: "3px solid #833fb2", width: "50%" }} />
            <h5>Addresse</h5>
            <p></p>
            <h5>Phone</h5>
            <p>0681968004</p>
            <h5>Email</h5>
            <p>t.kadiata@hotmail.com ou t.kaadiata@gmail.com</p>
            <hr style={{ borderTop: "3px solid #833fb2", width: "50%" }} />
          </Cell>
          <Cell className="resume-right-col" col={8}>
            <h2>Formation ou Diplôme</h2>

            <Education
              startYear={2019}
              endYear={2020}
              schoolName="3wa academy"
              schoolDescription="Une école ou il t'apprendre a devenir développeur web fullstack etc"
            />

            <Education
              startYear={2015}
              endYear={2016}
              schoolName="Paris 13 villetaneuse"
              schoolDescription="A paris 13 on apprendre a faire des maths et de informatique niveau programmation"
            />
            <hr style={{ borderTop: "3px solid #e22947" }} />

            <h2>Experience</h2>

            <Experience
              startYear={2019}
              endYear={2019}
              jobName="Monteur cableur "
              jobDescription="Le monteur-câbleur construit et répare des matériels électriques et électroniques à partir de plans et schémas"
            />

            <Experience
              startYear={2014}
              endYear={2015}
              jobName="Stage Franprix"
              jobDescription="Je réalise le vente de produits (décoration, équipement du foyer, produits alimentaires, vêtements, etc.) auprès d'une clientèle."
            />
            <hr style={{ borderTop: "3px solid #e22947" }} />
            <h2 className="Center">Compétence</h2>
            {/*<Skills skill="javascript" />
            <Skills skill="HTML/CSS" />
            <Skills skill="PHP" />
            <Skills skill="React" />
            <Skills skill="Git" />
            <Skills skill="Vue" />
            <Skills skill="Nodejs" />
            <Skills skill="Mysql" /> */}
            <h3 className="Center">HTML/CSS</h3>
            <h3 className="Center">javascript</h3>
            <h3 className="Center">PHP</h3>
            <h3 className="Center">React</h3>
            <h3 className="Center">Git</h3>
            <h3 className="Center">Vuejs</h3>
            <h3 className="Center">Nodejs</h3>
            <h3 className="Center">Mysql</h3>
          </Cell>
        </Grid>
      </div>
    );
  }
}

export default Parcours;
