import React, { Component } from "react";
import Pdf from "../Triven Kadiata cv Développeur.pdf";
import { Grid } from "react-mdl";

class About extends Component {
  render() {
    return (
      <div className="contact-body">
        <Grid className="contact-grid">
          <h1>A propos de moi </h1>
          <p>
            Je m’appelle Triven Kadiata, j'habite en Ile de France.J'ai effectué
            une formation développeur web. j'aime expérimenter, découvrir et
            apprendre des nouvelles technologies. J'ai acquis des compétences en
            développement web .Dans ce portfolio que j'ai réalisé, je vais vous
            présenter mes compétences, mon parcours et projets. Le
            développement, les high-tech , musique , jeux vidéo , anime, manga ,
            série , sont les plus grandes passions et j’aime y consacrer mon
            temps libre. Pour toute question, n’hésitez pas à me contacter :)
          </p>
          <a href={Pdf} target="_blank">
            Cliquez ici pour voir mon cv
          </a>
        </Grid>
      </div>
    );
  }
}

export default About;
