import React from "react";
import { Switch, Route } from "react-router-dom";

import Home from "./accueil";
import AboutMe from "./aboutme";
import Contact from "./contact.js";
import Projects from "./projects";
import Parcours from "./parcours";

const Main = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/aboutme" component={AboutMe} />
    <Route path="/contact" component={Contact} />
    <Route path="/projects" component={Projects} />
    <Route path="/parcours" component={Parcours} />
  </Switch>
);

export default Main;
