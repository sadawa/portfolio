import React, { Component } from "react";
import {
  Tabs,
  Tab,
  Grid,
  Cell,
  Card,
  CardTitle,
  CardText,
  CardActions,
  Button,
  CardMenu,
  IconButton,
} from "react-mdl";
import tayo from "../japon-pays-soleil-levant.jpg";
import lofi from "../lofi.jpg";
import sky from "../sky.jpg";
import anime from "../otakugeek.png";
import ichigo from "../ichigo.jpg";
import port from "../portfolio.png";

class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: 0 };
  }

  toggleCategories() {
    if (this.state.activeTab === 0) {
      return (
        <div className="projects-grid">
          {/* Project 1 */}
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background: "url(" + port + ")",
                backgroundSize: "cover",
              }}
            >
              Portfolio
            </CardTitle>
            <CardText>Cree un portfolio avec reactjs </CardText>
            <CardActions border>
              <Button colored>GitHub</Button>
              <Button colored>
                <a href=""></a>
              </Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>

          {/* Project 2 */}
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background:
                  "url(https://xtnotes-1255646395.coshk.myqcloud.com/images/react-1.svg) center / cover",
              }}
            >
              React Project #2
            </CardTitle>
            <CardText>soon</CardText>
            <CardActions border>
              <Button colored>GitHub</Button>
              <Button colored>Live Demo</Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>

          {/* Project 3 */}
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background:
                  "url(https://xtnotes-1255646395.coshk.myqcloud.com/images/react-1.svg) center / cover",
              }}
            >
              React Project #3
            </CardTitle>
            <CardText>Soon</CardText>
            <CardActions border>
              <Button colored>GitHub</Button>
              <Button colored>Live Demo</Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>
        </div>
      );
    } else if (this.state.activeTab === 1) {
      return (
        <div className="Back">
          <h1 className="Langages">Nodejs</h1>
          {/* Project 1 */}
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background: "url(" + ichigo + ")",
                backgroundSize: "cover",
              }}
            >
              <h2 className="Title">Discord</h2>
            </CardTitle>
            <CardText>
              Cree un bot qui pourra des fonction lire des musique youtube ou
              faire des commandes (en cours )
            </CardText>
            <CardActions border>
              <Button colored>
                <a href="https://gitlab.com/sadawa/discord">GitHub</a>
              </Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>

          {/* Project 2 */}
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background:
                  "url(https://upload.wikimedia.org/wikipedia/commons/d/d9/Node.js_logo.svg) center / cover",
              }}
            >
              <h2 className="Title">Chat</h2>
            </CardTitle>
            <CardText>Cree un mini chat avec nodejs (en cours)</CardText>
            <CardActions border>
              <Button colored>
                <a href="https://gitlab.com/sadawa/node">GitHub</a>
              </Button>
              <Button colored>
                <a href="https://chatndjs.herokuapp.com/">Lien direct</a>
              </Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>
        </div>
      );
    } else if (this.state.activeTab === 2) {
      return (
        <div className="Back">
          <h1 className="Langages">Vuejs</h1>
          {/* Project 1 */}
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background: "url(" + lofi + ")",
                backgroundSize: "cover",
                backgroundPosition: "center",
                backgroundAttachment: "fixed",
              }}
            >
              <h2 className="Title">Music</h2>
            </CardTitle>
            <CardText>Cree une playlist avec vuejs genre lofi</CardText>
            <CardActions border>
              <Button colored>
                <a href="https://gitlab.com/sadawa/msuic">gitlab</a>
              </Button>
              <Button colored>
                {" "}
                <a href="https://playlistlofi.netlify.app">Lien direct</a>
              </Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>
          {/* Project 2 */}
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background: "url(" + sky + ")",
                backgroundSize: "cover",
                backgroundPosition: "center",
                backgroundAttachment: "fixed",
              }}
            >
              <h2 className="Title">Méteo</h2>
            </CardTitle>
            <CardText>Cree une application meteo avec une api</CardText>
            <CardActions border>
              <Button colored>
                <a href="https://gitlab.com/sadawa/m-t-o">gitlab</a>
              </Button>
              <Button colored>
                <a href="https://meteoappli.netlify.app">Lien direct</a>
              </Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>
        </div>
      );
    } else if (this.state.activeTab === 3) {
      return (
        <div className="Back">
          <h1 className="Langages">Php</h1>
          {/* Project 1 */}
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background: "url(" + anime + ")",
              }}
            >
              <h2 className="Title">Animegeek</h2>
            </CardTitle>
            <CardText>
              Un projet qui pour objectif de faire un site le theme que j'ai
              pris otaku et anime.
            </CardText>
            <CardActions border>
              <Button colored>
                <a href="https://gitlab.com/sadawa/projet-final">GitHub</a>
              </Button>
              <Button colored>
                <a href="http://animegeek.epizy.com/">Lien direct</a>
              </Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>
        </div>
      );
    } else if (this.state.activeTab === 4) {
      return (
        <div className="Back">
          <h1 className="Langages">HTML/CSS</h1>
          {/* Project 1 */}
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background: "url(" + tayo + ")",
                backgroundSize: "cover",
                backgroundPosition: "center",
                backgroundAttachment: "fixed",
              }}
            >
              <h2 className="Title">Maquette kaze</h2>
            </CardTitle>
            <CardText>Site en cours </CardText>
            <CardActions border>
              <Button colored>
                <a href="https://gitlab.com/sadawa/maquette">gitlab</a>
              </Button>
              <Button colored>
                {" "}
                <a
                  href="
              https://kazetaiyo.netlify.app"
                >
                  Lien direct
                </a>
              </Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>
        </div>
      );
    }
  }

  render() {
    return (
      <div>
        <Tabs
          activeTab={this.state.activeTab}
          onChange={(tabId) => this.setState({ activeTab: tabId })}
          ripple
        >
          <Tab>React</Tab>
          <Tab>Nodejs</Tab>
          <Tab>VueJS</Tab>
          <Tab>PHP</Tab>
          <Tab>HTML/CSS</Tab>
        </Tabs>

        <Grid>
          <Cell col={12}>
            <div className="content">{this.toggleCategories()}</div>
          </Cell>
        </Grid>
      </div>
    );
  }
}

export default Projects;
